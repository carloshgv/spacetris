﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {
	public Transform followee;

	private Vector3 position;

	// Use this for initialization
	void Start () {
		position = transform.position - followee.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Euler(Mathf.Sin(Time.time)*0.01f, 
											  Mathf.Cos(Time.time)*0.01f, 
											  Mathf.Sin(Time.time)*0.01f) * transform.rotation;
		transform.position = followee.position + position;
	}
}

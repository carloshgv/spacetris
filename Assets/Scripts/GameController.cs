﻿using UnityEngine;
using System;
using System.Collections;

public class GameController : MonoBehaviour {
	public GUIText guiPlayTime;
	public GameObject shipPrefab;
	public Platform platform;	

	private AudioSource music;
	private TrackController trackController;
	private GameObject player;
	private float playTime;
	private GameObject mainCamera;
	private bool dof;

	public void RestartLevel() {
		playTime = 0;
		Destroy(GameObject.FindWithTag("Player"));
		trackController.Reset();
		player = Instantiate(shipPrefab, trackController.transform.position + trackController.transform.rotation * Vector3.up * 2, 
								  		 trackController.transform.rotation) as GameObject;
		player.transform.parent = trackController.transform;
		music = GetComponents<AudioSource>()[UnityEngine.Random.Range(0, GetComponents<AudioSource>().Length)];
		music.volume = Settings().musicVolume;
		music.Play();		
	}

	void Start() {
		//platform = Platform.PC;
		//if(Application.platform == RuntimePlatform.Android)
		//	platform = Platform.ANDROID;
		trackController = GameObject.FindWithTag("TrackController").GetComponent<TrackController>();
		player = GameObject.FindWithTag("Player");
		mainCamera = GameObject.FindWithTag("MainCamera");
		playTime = 0;
		dof = Settings().dof;
		mainCamera.GetComponent<IndieEffects>().enabled = dof;
		mainCamera.GetComponent<DepthOfField>().enabled = dof;
		music = GetComponents<AudioSource>()[UnityEngine.Random.Range(0, GetComponents<AudioSource>().Length)];
		music.volume = Settings().musicVolume;
		music.Play();
	}

	private string SecondsToTime(float input) {
		int minutes, seconds, fraction;

		fraction = (int)((input - (int)input) * 100f);
		seconds = (int)input % 60;
		minutes = ((int)input - seconds) / 60;

		return String.Format("{0,2:d2}:{1,2:d2}.{2,2:d2}", minutes, seconds, fraction);
	}

	void Update() {
		if(trackController.state == State.ALIVE)
			playTime += Time.deltaTime;
		guiPlayTime.text = SecondsToTime(playTime);
		if(Input.GetButtonDown("Jump")) {
			dof = !dof;
			mainCamera.GetComponent<IndieEffects>().enabled = dof;
			mainCamera.GetComponent<DepthOfField>().enabled = dof;
		}
	}

	public AudioSource Music() {
		return music;
	}

	public Settings Settings() {
		return Array.Find(GetComponents<Settings>(), (s) => s.platform == platform);
	}
}

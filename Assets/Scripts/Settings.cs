﻿using UnityEngine;
using System.Collections;

public enum Platform {
	PC,
	ANDROID
}

public class Settings : MonoBehaviour {
	public Platform platform;
	public int numSegments;
	public float startSpeed;
	public float maxSpeed;
	public float acceleration;
	public float trackDensity;
	public float extendThreshold;
	public float startWallsAt;
	public float deathSpeed;
	public float explosionForce;
	public float steeringSpeed;
	public float tilt;
	public float wallDistance;
	public bool dof;
	[Range(0, 1)]
	public float musicVolume;
	public float worldToParam;
}

﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {
	public GameObject sharpnel;
	public GameObject explosion;

	private TrackController trackController;
	private GameController gameController;
	private Settings settings;
	private	float moveHorizontal;

	// Use this for initialization
	void Start () {
		trackController = GameObject.FindWithTag("TrackController").GetComponent<TrackController>();
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
		settings = gameController.GetComponent<Settings>();
		moveHorizontal = 0;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Wall") {
			Debug.Log("kabooom");
			trackController.state = State.DEAD;
			GameObject temp = new GameObject();
			temp.name = "explosion";
			temp.tag = "Player";
			temp.transform.position = transform.position;
			temp.transform.rotation = transform.parent.rotation;
			temp.transform.parent = transform.parent;
			GameObject s = Instantiate(sharpnel, transform.position, transform.rotation) as GameObject;
			s.transform.parent = temp.transform;
			GameObject e = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
			e.transform.parent = temp.transform;
			Destroy(gameObject);
		}
		if(other.tag == "Goal") {
			Debug.Log("vroom vroooom!!!");
			trackController.speed = Mathf.Lerp(trackController.speed, settings.maxSpeed, settings.acceleration);
		}

	}

	// Update is called once per frame
	void FixedUpdate () {
		if(Input.touchCount > 0) {
			float dest = (Input.GetTouch(0).position.x > Screen.width/2) ? 1f : -1f;
			moveHorizontal = Mathf.Lerp(moveHorizontal, dest, 0.2f);
			Debug.Log(moveHorizontal);
		} else {
			moveHorizontal = Input.GetAxis ("Horizontal");
		}
		Vector3 movement = new Vector3 (moveHorizontal, Mathf.Sin(Time.time*2)*0.005f, 0.0f);

		rigidbody.velocity = transform.parent.rotation * movement * (settings.steeringSpeed + trackController.speed*200);
		rigidbody.rotation = Quaternion.Slerp(rigidbody.rotation,
											transform.parent.rotation * Quaternion.Euler(0.0f, 
											  				   		     movement.x * settings.steeringSpeed * settings.tilt, 
											  				   			 movement.x * settings.steeringSpeed * -settings.tilt),
											 				  			 0.3f);
	}
}

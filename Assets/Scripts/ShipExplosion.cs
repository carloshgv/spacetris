﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipExplosion : MonoBehaviour {
	private GUITexture flash;
	private float alpha = 0.8f;

	void Start () {
		Settings settings = GameObject.FindWithTag("GameController").GetComponent<Settings>();
		foreach(Transform t in transform) {
		    t.gameObject.rigidbody.AddForce(transform.rotation * 
										    (t.position - transform.position)*settings.explosionForce * 
										    Random.Range(0.5f, 1f), ForceMode.Impulse);
			t.gameObject.rigidbody.AddForce(transform.rotation * 
											Vector3.forward*settings.explosionForce * 1.5f, ForceMode.Impulse);
		}
		flash = new GameObject("flash", typeof(GUITexture)).GetComponent<GUITexture>();
		flash.pixelInset = new Rect(0, 0, Screen.width, Screen.height);
		Texture2D tex = new Texture2D(1, 1);
		tex.SetPixel(0, 0, new Color(1, 1, 1, alpha));
		tex.Apply();
		flash.texture = tex;
	}

	void Update() {
		alpha = Mathf.Lerp(alpha, 0, 4*Time.deltaTime);
		(flash.texture as Texture2D).SetPixel(0, 0, new Color(1, 1, 1, alpha));
		(flash.texture as Texture2D).Apply();
		foreach(Transform t in transform) {
			t.gameObject.rigidbody.AddForce(transform.parent.rotation * Vector3.down * 9.8f, ForceMode.Force);
		}
	}

	void OnDestroy() {
		if(flash.gameObject)
			Destroy(flash.gameObject);
	}
}

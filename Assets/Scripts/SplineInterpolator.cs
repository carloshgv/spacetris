using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void OnEndCallback();

public class SplineInterpolator : MonoBehaviour {
	internal class SplineNode {
		internal Vector3 Point;
		internal Quaternion Rot;
		internal float Time;
		internal Vector2 EaseIO;
		
		internal SplineNode(Vector3 p, Quaternion q, float t, Vector2 io) {
			Point = p;
			Rot = q;
			Time = t;
			EaseIO = io;
		}

		internal SplineNode(SplineNode o) {
			Point = o.Point;
			Rot = o.Rot;
			Time = o.Time;
			EaseIO = o.EaseIO;
		}
	}
	
	List<SplineNode> mNodes = new List<SplineNode>();

	public Quaternion GetInitialRotation() {
		return mNodes[0].Rot;
	}

	public void Reset() {
		mNodes.Clear();
	}

	public void AddPoint(Vector3 pos, Quaternion quat, float timeInSeconds, Vector2 easeInOut) {
		if(mNodes.Count == 0)
			mNodes.Add(new SplineNode(pos, quat, timeInSeconds, easeInOut));
		mNodes.Add(new SplineNode(pos, quat, timeInSeconds, easeInOut));
	}

	Quaternion GetSquad(int idxFirstPoint, float t) {
		Quaternion Q0 = mNodes[idxFirstPoint - 1].Rot;
		Quaternion Q1 = mNodes[idxFirstPoint].Rot;
		Quaternion Q2 = mNodes[idxFirstPoint + 1].Rot;
		Quaternion Q3 = mNodes[idxFirstPoint + 2].Rot;
		
		Quaternion T1 = MathUtils.GetSquadIntermediate(Q0, Q1, Q2);
		Quaternion T2 = MathUtils.GetSquadIntermediate(Q1, Q2, Q3);
		
		return MathUtils.GetQuatSquad(t, Q1, Q2, T1, T2);
	}
	
	public Vector3 GetHermiteInternal(int idxFirstPoint, float t) {
		float t2 = t * t;
		float t3 = t2 * t;
		
		Vector3 P0 = mNodes[idxFirstPoint - 1].Point;
		Vector3 P1 = mNodes[idxFirstPoint].Point;
		Vector3 P2 = mNodes[idxFirstPoint + 1].Point;
		Vector3 P3 = mNodes[idxFirstPoint + 2].Point;
		
		float tension = 0.5f;	// 0.5 equivale a catmull-rom
		
		Vector3 T1 = tension * (P2 - P0);
		Vector3 T2 = tension * (P3 - P1);
		
		float Blend1 = 2 * t3 - 3 * t2 + 1;
		float Blend2 = -2 * t3 + 3 * t2;
		float Blend3 = t3 - 2 * t2 + t;
		float Blend4 = t3 - t2;
		
		return Blend1 * P1 + Blend2 * P2 + Blend3 * T1 + Blend4 * T2;
	}

	public Vector3 GetTangentInternal(int idxFirstPoint, float t) {
		float t2 = t * t;
		
		Vector3 P0 = mNodes[idxFirstPoint - 1].Point;
		Vector3 P1 = mNodes[idxFirstPoint].Point;
		Vector3 P2 = mNodes[idxFirstPoint + 1].Point;
		Vector3 P3 = mNodes[idxFirstPoint + 2].Point;
		
		float tension = 0.5f;	// 0.5 equivale a catmull-rom
		
		Vector3 T1 = tension * (P2 - P0);
		Vector3 T2 = tension * (P3 - P1);
		
		float Blend1 = 6 * t2 - 6 * t;
		float Blend2 = -6 * t2 + 6 * t;
		float Blend3 = 3 * t2 - 4 * t + 1;
		float Blend4 = 3 * t2 - 2 * t;
		
		return Blend1 * P1 + Blend2 * P2 + Blend3 * T1 + Blend4 * T2;
	}

	int GetIdx(float timeParam) {
		int c;
		for(c = 1; c < mNodes.Count - 2; c++) {
			if(mNodes[c].Time > timeParam)
				break;
		}
		
		return c - 1;
	}
	
	public Vector3 GetTangentAtTime(float timeParam) {
		int idx = GetIdx(timeParam);
		float param = (timeParam - mNodes[idx].Time) / (mNodes[idx + 1].Time - mNodes[idx].Time);
		param = MathUtils.Ease(param, mNodes[idx].EaseIO.x, mNodes[idx].EaseIO.y);		
		return GetTangentInternal(idx, param).normalized;
	}

	public Vector3 GetHermiteAtTime(float timeParam) {
		if(timeParam >= mNodes[mNodes.Count - 2].Time)
			return mNodes[mNodes.Count - 2].Point;

		int idx = GetIdx(timeParam);
		float param = (timeParam - mNodes[idx].Time) / (mNodes[idx + 1].Time - mNodes[idx].Time);
		param = MathUtils.Ease(param, mNodes[idx].EaseIO.x, mNodes[idx].EaseIO.y);
		
		return GetHermiteInternal(idx, param);
	}

	public Quaternion GetSquadAtTime(float timeParam) {
		if(timeParam >= mNodes[mNodes.Count - 2].Time)
			return mNodes[mNodes.Count - 2].Rot;

		int idx = GetIdx(timeParam);
		float param = (timeParam - mNodes[idx].Time) / (mNodes[idx + 1].Time - mNodes[idx].Time);
		param = MathUtils.Ease(param, mNodes[idx].EaseIO.x, mNodes[idx].EaseIO.y);
		return GetSquad(idx, param);
	}
}

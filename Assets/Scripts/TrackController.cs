﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum State {
	ALIVE,
	DEAD
};

public class TrackController : MonoBehaviour {
	public TrackSegment[] segments;
	public List<TrackSegment> track;
	public Pattern[] patterns;
	public Vector3 spawnPosition;
	public State state;
	public float speed;
	
	private GameController gameController;
	private Settings settings;
	private float duration;
	private float t;
	private float lastPatternTime;
	private GameObject trackRoot;

	private float[] segmentProbs;
	private void ComputeSegmentProbs() {
		segmentProbs = new float[segments.Length];
		segmentProbs[0] = segments[0].probability;
		for(int i = 1; i < segmentProbs.Length; i++)
			segmentProbs[i] = segmentProbs[i-1] + segments[i].probability;
		for(int i = 0; i < segmentProbs.Length; i++)
			segmentProbs[i] /= segmentProbs[segmentProbs.Length-1];
	}

	private TrackSegment RandomSegment() {
		if(segmentProbs == null)
			ComputeSegmentProbs();

		if(duration < 0.01f)
			return segments[0];
		float p = Random.value;
		int i;
		for(i = 0; i < segmentProbs.Length; i++)
			if(p <= segmentProbs[i])
				break;
		return segments[i];
	}

	//private float[] patternProbs;
	private Pattern RandomPattern() {
		return patterns[Random.Range(0, patterns.Length)];
	}

	public void Reset() {
		duration = 0;
		speed = settings.startSpeed;
		lastPatternTime = settings.startWallsAt;
		t = 1;
		state = State.ALIVE;
		InitTrack();
		SetupWalls();
		transform.rotation = Quaternion.identity;
		UpdatePosition();
		t = 1;		
	}

	void Start() {
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
		settings = gameController.Settings();
		foreach(TrackSegment s in segments)
			s.Init();		
		Reset();
	}

	private void InitTrack() {
		trackRoot = new GameObject("track") as GameObject;
		Vector3 prevEndPos = spawnPosition;
		Quaternion prevEndRot = Quaternion.identity;
		float t = 0;
		for(int i = 0; i < settings.numSegments; i++) {
			TrackSegment seg = RandomSegment();
			GameObject obj = new GameObject(seg.segName, typeof(TrackSegment), typeof(SplineInterpolator), typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider)) as GameObject;
			TrackSegment objTs = obj.GetComponent<TrackSegment>();
			objTs.Copy(seg);
			objTs.transform.position = prevEndPos;
			objTs.transform.rotation = prevEndRot;
			objTs.transform.parent = trackRoot.transform;
			objTs.st = t;
			objTs.et = t + objTs.duration;
			objTs.gameObject.SetActive(true);
			prevEndPos = prevEndPos + prevEndRot * seg.waypoints[seg.waypoints.Count - 1].transform.position;
			prevEndRot = prevEndRot * seg.waypoints[seg.waypoints.Count - 1].transform.rotation;
			t += objTs.duration;
			duration = t;
			obj.GetComponent<MeshFilter>().mesh = seg.meshNear;
			obj.GetComponent<MeshRenderer>().material = seg.mat;
			track.Add(objTs);
		}
	}

	private void SetupWalls() {
		float currT = lastPatternTime;
		while(currT < t + settings.wallDistance) {
			Pattern pattern = RandomPattern();
			GameObject pattGo = Instantiate(pattern.gameObject) as GameObject;
			float baseT = currT;
			List<Transform> children = new List<Transform>();
			foreach(Transform w in pattGo.transform) children.Add(w);
			foreach(Transform w in children) {
				currT = baseT + w.position.z/settings.worldToParam;
				TrackSegment s = GetCurrentSegment(currT);				
				Vector3 position = s.transform.position + s.transform.rotation * s.interp.GetHermiteAtTime(currT-s.st);
		    	Quaternion rotation = s.transform.rotation * s.interp.GetSquadAtTime(currT-s.st);
		    	Vector3 tangent = s.transform.rotation * s.interp.GetTangentAtTime(currT-s.st);
		    	Vector3 normal = rotation * Vector3.up;		    	
				w.position = position;
				w.rotation = Quaternion.LookRotation(-tangent, normal);
				w.parent = s.transform;
			}
			currT += pattern.separation/settings.worldToParam;
			Destroy(pattGo);
		}
		lastPatternTime = currT;
	}

	void UpdatePosition() {
		TrackSegment s = GetCurrentSegment(t);
		float param = Mathf.Lerp(t-s.st, (t-s.st)+speed, speed*Time.deltaTime);
		Vector3 newPosition = s.transform.position + s.transform.rotation * s.interp.GetHermiteAtTime(param);
		Vector3 normal = s.transform.rotation * s.interp.GetSquadAtTime(param) * Vector3.up;
		Vector3 tangent = s.transform.rotation * s.interp.GetTangentAtTime(t-s.st);
		Quaternion newRotation = Quaternion.LookRotation(tangent, normal);
		transform.rotation = newRotation;
		transform.position = newPosition;

		t+=speed;
	}

	void AliveUpdate () {
		UpdatePosition();
		if(t > duration - settings.extendThreshold)
			ExtendTrack();
		if(lastPatternTime - t < 25)
			SetupWalls();
	}

	void DeadUpdate() {
		UpdatePosition();
		speed = Mathf.Lerp(speed, 0, settings.deathSpeed);
		gameController.Music().volume = Mathf.Lerp(gameController.Music().volume, 0, 2f*Time.deltaTime);
		if(speed < 0.00001f) {
			Destroy(trackRoot.gameObject);
			track.Clear();
			gameController.Music().Stop();			
			gameController.RestartLevel();
			//Application.LoadLevel(Application.loadedLevel);			
		}
	}

	void FixedUpdate() {
		switch(state) {
		case State.ALIVE:
			AliveUpdate();
			break;
		case State.DEAD:
			DeadUpdate();
			break;
		}
	}

	private void ExtendTrack() {
		TrackSegment newSeg = RandomSegment();
		TrackSegment first = track[0], last = track[track.Count - 1];
		foreach(Transform t in first.transform)
			Destroy(t.gameObject);
		Transform insertPoint = last.waypoints[last.waypoints.Count - 1];
		track.RemoveAt(0);

		first.Copy(newSeg);
		first.transform.position = last.transform.position + last.transform.rotation * insertPoint.transform.position;
		first.transform.rotation = last.transform.rotation * insertPoint.transform.rotation;
		first.transform.parent = last.transform.parent;
		first.st = last.et;
		first.et = first.st + first.duration;
		duration = first.et;
		track.Add(first);
	}

	private TrackSegment GetCurrentSegment(float t) {
		foreach(TrackSegment s in track)
			if(s.st <= t && t < s.et)
				return s;
		Debug.LogError("segment not found");
		return null;
	}
}

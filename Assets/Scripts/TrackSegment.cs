using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SplineInterpolator))]
public class TrackSegment : MonoBehaviour {
	public List<Transform> waypoints;
	public string segName;
	public float duration;
	public SplineInterpolator interp;
	public float st, et;
	public int probability;
	public Mesh meshNear, meshMiddle, meshFar;
	public Material mat;

	private TrackController trackController;
	private MeshFilter meshFilter;
	private MeshCollider meshCollider;
	
	void OnDrawGizmos() {
		waypoints.Clear();
		foreach(Transform t in transform) {
			Gizmos.DrawSphere(transform.position + transform.rotation * t.position, 5);
			waypoints.Add(t);
		}
		SplineInterpolator interp = GetComponent(typeof(SplineInterpolator)) as SplineInterpolator;
		SetupSplineInterpolator(interp, waypoints.ToArray());
		float density = 0.02f;
		Vector3 prevPos = transform.position + waypoints[0].transform.position;
		float currTime = 0;
		for(currTime = 0; currTime <= duration; currTime+=density) {
			Vector3 currPos = transform.position + transform.rotation * interp.GetHermiteAtTime(currTime);
		    Quaternion currRot = transform.rotation * interp.GetSquadAtTime(currTime);
		    Vector3 tangent = interp.GetTangentAtTime(currTime);
		    Vector3 normal = currRot * Vector3.up;
		    Vector3 offset = Vector3.Cross(normal, tangent).normalized * 10;
			float mag = (currPos - prevPos).magnitude * 2;
			Gizmos.color = new Color(mag, 0, 0, 1);
			Gizmos.DrawLine(currPos - offset, currPos + offset);

			Gizmos.color = new Color(mag, 0, 1, 1);
			//Gizmos.DrawRay (currPos, Vector3.Cross((currPos - offset) - (currPos + offset), (currPos - offset) - (prevPos - offset)).normalized * 5);
			Gizmos.DrawRay (currPos, normal * 5);
			Gizmos.color = new Color(mag, 1, 1, 1);			
			Gizmos.DrawRay (currPos, tangent * 5);
			prevPos = currPos;
		}
		/*if(Mathf.Abs(currTime - duration) > 1e-6f) {
			currTime = duration - 1e-6f;
			Vector3 currPos = transform.position + interp.GetHermiteAtTime(currTime);
		    Quaternion currRot = transform.rotation * interp.GetSquadAtTime(currTime);
		    Vector3 tangent = currRot * interp.GetTangentAtTime(currTime);
		    Vector3 normal = currRot * Vector3.up;
		    Vector3 offset = Vector3.Cross(normal, tangent).normalized * 10;
			float mag = (currPos - prevPos).magnitude * 2;
			Gizmos.color = new Color(mag, 0, 0, 1);
			Gizmos.DrawLine(currPos - offset, currPos + offset);

			Gizmos.color = new Color(mag, 0, 1, 1);
			Gizmos.DrawRay (currPos, Vector3.Cross((currPos - offset) - (currPos + offset), (currPos - offset) - (prevPos - offset)).normalized * 5);			
		}*/
	}

	public void Init() {
		interp = GetComponent<SplineInterpolator>();
		trackController = GameObject.FindWithTag("TrackController").GetComponent<TrackController>();
		meshFilter = GetComponent<MeshFilter>();
		meshCollider = GetComponent<MeshCollider>();
		SetupSplineInterpolator(interp, waypoints.ToArray());			
	}

	#if UNITY_EDITOR
	[ContextMenu("Build Meshes")]
	void BuildMeshes() {
		Settings settings = GameObject.Find("game_controller").GetComponent<GameController>().Settings();
		SetupSplineInterpolator(interp, waypoints.ToArray());	
		meshNear = CreateMesh(settings.trackDensity);
		AssetDatabase.DeleteAsset("Assets/Models/Segments/" + meshNear.name + " near.asset");
		AssetDatabase.CreateAsset(meshNear, "Assets/Models/Segments/" + meshNear.name + " near.asset");
		meshMiddle = CreateMesh(settings.trackDensity * 2, 3);
		AssetDatabase.DeleteAsset("Assets/Models/Segments/" + meshMiddle.name + " middle.asset");
		AssetDatabase.CreateAsset(meshMiddle, "Assets/Models/Segments/" + meshMiddle.name + " middle.asset");
		meshFar = CreateMesh(settings.trackDensity * 16, 3);
		AssetDatabase.DeleteAsset("Assets/Models/Segments/" + meshFar.name + " far.asset");
		AssetDatabase.CreateAsset(meshFar, "Assets/Models/Segments/" + meshFar.name + " far.asset");
		AssetDatabase.SaveAssets();
	}

	Mesh CreateMesh(float density, int stride = 1) {
		gameObject.name = segName + " segment";
		gameObject.SetActive(false);
		gameObject.layer = 8;

		Mesh profile = Resources.Load("track_profile", typeof(Mesh)) as Mesh;
	
		Mesh mesh = new Mesh();
		mesh.name = segName + " mesh";

		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> triangles = new List<int>();
		int profileVerticesLength = 51/stride;//profile.vertices.Length / 2 / stride;
		for(int i = 0; i < profileVerticesLength; i++) {
			vertices.Add(profile.vertices[i*stride]);
			uvs.Add(new Vector2(profile.uv[i*stride].x, 0));
		}

		// Build mesh
		Vector3 currPos = Vector3.zero;
		int loop = 0;
		float currTime;
		for(currTime = 0; currTime <= duration; currTime += density) {
			currPos = interp.GetHermiteAtTime(currTime);
		    Quaternion currRot = interp.GetSquadAtTime(currTime);
		    Vector3 tangent = interp.GetTangentAtTime(currTime);
		    Vector3 normal = currRot * Vector3.up;
		    for(int i = 0; i < profileVerticesLength; i++) {
				vertices.Add(currPos + Quaternion.LookRotation(tangent, normal) * profile.vertices[i*stride]);
				uvs.Add(new Vector2(profile.uv[i*stride].x, (density/10)*loop+(density/10)));
			}
			for(int i = loop; i < loop+profileVerticesLength-1; i++) {
				triangles.AddRange(new int[]{i+0, i+profileVerticesLength, i+1, i+1, i+profileVerticesLength, i+profileVerticesLength+1});
			}
			triangles.AddRange(new int[]{vertices.Count - profileVerticesLength - 1, 
										 vertices.Count - 1, 
										 vertices.Count - profileVerticesLength - profileVerticesLength,
										 vertices.Count - 1, 
										 vertices.Count - profileVerticesLength, 
										 vertices.Count - profileVerticesLength - profileVerticesLength});
			loop += profileVerticesLength;
		}
		if(Mathf.Abs(currTime - duration) > 1e-6f) {
			currTime = duration - 1e-6f;
			currPos = interp.GetHermiteAtTime(currTime);
		    Quaternion currRot = interp.GetSquadAtTime(currTime);
		    Vector3 tangent = interp.GetTangentAtTime(currTime);
		    Vector3 normal = currRot * Vector3.up;
		    for(int i = 0; i < profileVerticesLength; i++) {
				vertices.Add(currPos + Quaternion.LookRotation(tangent, normal) * profile.vertices[i*stride]);
				uvs.Add(new Vector2(profile.uv[i*stride].x, (density/10)*loop+(density/10)));
			}
			for(int i = loop; i < loop+profileVerticesLength-1; i++) {
				triangles.AddRange(new int[]{i+0, i+profileVerticesLength, i+1, i+1, i+profileVerticesLength, i+profileVerticesLength+1});
			}
			triangles.AddRange(new int[]{vertices.Count - profileVerticesLength - 1, 
										 vertices.Count - 1, 
										 vertices.Count - profileVerticesLength - profileVerticesLength,
										 vertices.Count - 1, 
										 vertices.Count - profileVerticesLength, 
										 vertices.Count - profileVerticesLength - profileVerticesLength});
		}

		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.uv = uvs.ToArray();
		mesh.RecalculateNormals();
		return mesh;
	}	
	#endif
	
	public void SetupSplineInterpolator(SplineInterpolator interp, Transform[] waypoints) {
		interp.Reset();
		duration = 0;
		int c;
		interp.AddPoint(waypoints[0].position, waypoints[0].rotation, duration, new Vector2(0, 1));
		interp.AddPoint(waypoints[0].position, waypoints[0].rotation, duration, new Vector2(0, 1));
		for(c = 1; c < waypoints.Length; c++) {
			duration += (waypoints[c].position - waypoints[c-1].position).magnitude / 100;						
			interp.AddPoint(waypoints[c].position, waypoints[c].rotation, duration, new Vector2(0, 1));
		}
		interp.AddPoint(waypoints[--c].position, waypoints[c].rotation, duration, new Vector2(0, 1));
	}

	public void Copy(TrackSegment other) {
		meshNear = other.meshNear;
		meshMiddle = other.meshMiddle;
		meshFar = other.meshFar;
		trackController = other.trackController;	
		waypoints = other.waypoints;
		transform.position = other.transform.position;
		transform.rotation = other.transform.rotation;
		transform.parent = other.transform.parent;
		name = other.name;		
		st = other.st;
		et = other.et;
		duration = other.duration;
		interp = other.interp;
		meshFilter = GetComponent<MeshFilter>();
		meshCollider = GetComponent<MeshCollider>();
		if(meshFilter) {
			meshFilter.mesh = meshNear;
			other.GetComponent<MeshRenderer>().material = mat;
		}	
	}

	void Update() {
		float distance = Vector3.Distance(transform.position, trackController.transform.position);
		if(distance > 2000) {
			meshCollider.sharedMesh = null;
			meshFilter.mesh = meshFar;
		} else if(distance > 1500) {
			meshCollider.sharedMesh = null;
			meshFilter.mesh = meshMiddle;
		} else {
			meshFilter.mesh = meshNear;
			meshCollider.sharedMesh = meshNear;
		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {
	IEnumerator TextureAnimator() {
		float offset = 0;
		for(;;) {
			renderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
			offset += 0.5f;
			yield return new WaitForSeconds(0.1f);
		}
	}

	void Start() {
		StartCoroutine(TextureAnimator());
	}
}
